# MapYourProperty Frontend Engineer Case Study

## Tech Stack

- [ReactJS](https://reactjs.org/)
- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
- [Redux](https://react-redux.js.org/)
- [Figma Design](https://www.figma.com/)
- [Ant Design](https://ant.design/)
- [Phosphor Icon](https://phosphoricons.com/)
- [Google Maps API](https://cloud.google.com/blog/products/maps-platform/address-geocoding-in-google-maps-api)
- [Docker](https://www.docker.com/)

## Features

- Collapsable Sidebar: Sidebar is dynamic which can open and close.
- Responsive Google Map Design: Map can fit to the enitre screen with respect to screen size and Collapsable Sidebar.
- Used React Reusable Components: Used Reusable React Components for desing few similar parts of components and passed information through props.
- Search Feature: This Features is implemented with Google Maps API. You can search any place around the globe via search bar and marker will be placed over there.
- Autocomplete places: This Feature provides the functionality to suggesting places to autocomplete.
- Docker: Built a Docker image and running a container

## Demo

- There are following main components are implemented as per the Figma Designs.
- Sales/Rent
- Official Plan
- Zoning
- You can navigate to these components by clicking on the icon in Sidebar.

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/krunal111797/mapyourproperty-casestudy.git
```

Go to the project directory

```bash
  cd mapyourproperty-casestudy
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run start
```

## Screenshots

![Sidebar](/uploads/045efd65f06dc50bd69c031bd77eafb2/Screen_Shot_2022-06-09_at_11.20.54_AM.png)
![Official Plan](/uploads/ed01752f300a677896f00dcee16f5a31/Screen_Shot_2022-06-09_at_11.21.05_AM.png)
![Sales/Rent](/uploads/e6891d5185a20bcf44136ac888ed7035/Screen_Shot_2022-06-09_at_11.21.21_AM.png)
![Zoning](/uploads/75aa6db5b3cde8be758f21408b04b6ea/Screen_Shot_2022-06-09_at_11.22.00_AM.png)
![Docker](/uploads/3574b888f8213a75aec9a84f8099f925/Screen_Shot_2022-06-09_at_11.42.54_AM.png)

## 🔗 Links

[![portfolio](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://krunal-97.github.io/MyPortfolio/)
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/krunal-97/)
